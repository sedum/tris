<?php
  //avvio la sessione
  session_start();

  // solo con login
  if (!array_key_exists("utente",$_SESSION)):
    header("Location: ./login.php");
    die();  // php muori qui!
    // exit();
  endif;

  // load game's functions
  require_once "./lib/common.php";

  //carico la partita!
	$partita=array();
	if (array_key_exists("partita",$_GET)):
		$partita=tris_carica_partita($_GET["partita"]);
	endif;
	if ($partita==array()):
		$partita=tris_nuova_partita();
	endif;

  $hash=tris_hash_partita($partita);

  echo json_encode($hash);
