<?php
	// constants
	define("Q4_TRIS_CMD","cmd");
	define("Q4_TRIS_ROW","row");
	define("Q4_TRIS_COL","col");
	define("Q4_TRIS_RESTART","restart");
	define("Q4_TRIS_RESET","reset");
	define("Q4_TRIS_MOVE","move");
	define("Q4_TRIS_MODIFY","modify");
	define("Q4_TRIS_BOARD","board");
	define("Q4_TRIS_WIN","win");
	define("Q4_TRIS_PLAYER_PC","pc");
	define("Q4_TRIS_PLAYER_ONE","X");
	define("Q4_TRIS_PLAYER_TWO","O");
	define("Q4_TRIS_PLAYER_NONE","");
	define("Q4_TRIS_JOIN","gioca");

function tris_analyse_get()
{
  global $partita;

	if (array_key_exists(Q4_TRIS_CMD,$_GET)):
		switch ($_GET[Q4_TRIS_CMD]):
			case Q4_TRIS_JOIN:
				//c'è un secondo giocatore?
				if ($partita["playero"]==""):
					$partita["playero"]=$_SESSION["utente"];
				endif;
				break;
      case Q4_TRIS_RESTART:
        //fai il riavvio della partita
        $partita=tris_nuova_partita();
        break;
		endswitch;
	endif;
}

function tris_analyse_post()
{
global $partita;

if (array_key_exists(Q4_TRIS_CMD,$_POST)):
  $cmd = $_POST[Q4_TRIS_CMD];
  switch ($cmd):
    case Q4_TRIS_RESTART:
      $partita=tris_nuova_partita();
			$codice=tris_salva_partita($partita);
			header("Location: partita.php?partita=".$codice);
			die();
      break;
    // case Q4_TRIS_RESET:
    //   reset_counters();
    //   $_SESSION["scacchiera"] = tris_scacchiera_vuota();
    //   break;
    case Q4_TRIS_MOVE:
      $row = filter_input(INPUT_POST, Q4_TRIS_ROW, FILTER_SANITIZE_NUMBER_INT);
      $col = filter_input(INPUT_POST, Q4_TRIS_COL, FILTER_SANITIZE_NUMBER_INT);
      $partita=tris_mossa($partita,$row,$col);
      break;
    case Q4_TRIS_PLAYER_PC:
      $_SESSION["pc"] = !$_SESSION["pc"];
      break;
  endswitch;
endif;
}

function tris_salva_partita($partita)
	{
		$codice=$partita["codice"];
		$jsonpartita=json_encode($partita);
		file_put_contents("./partite/".$codice.".json",$jsonpartita);

		return $codice;
	}

function tris_carica_partita($codice)
	{
		if (is_file("./partite/".$codice.".json")):
		$jsonpartita=file_get_contents("./partite/".$codice.".json");
		$partita=json_decode($jsonpartita,true);

		return $partita;
		endif;
	}

function tris_hash_partita($partita)
{
	$jsonpartita=json_encode($partita);
	$hash=hash("md5",$jsonpartita);

	return $hash;
}

function tris_hash_index()
{
	$lista=glob("./partite/*.json");
	$files=implode($lista);
	$hashindex=hash("md5",$files);

	return $hashindex;
}

function tris_elenca_partite()
	{
		$codici=array();
		$lista=glob("./partite/*.json");
		if ($lista !== false):
			foreach($lista as $nomefile):
				if (is_file($nomefile)):
					$codici[]=substr(basename($nomefile),0,-5);
				endif;
			endforeach;
		endif;
		return $codici;
	}

function tris_nuova_partita()
	{
		$partita=array();
		$partita["codice"]=uniqid();
		$partita["gioco"]=array_key_exists("gioco",$_POST) ? $_POST["gioco"] : "tris";
		if ($partita["gioco"]==="tris"):
			$partita["board"]=array_key_exists("board",$_POST) ? $_POST["board"] : 3;
			$partita["wincase"]=array_key_exists("wincase", $_POST) ? $_POST["wincase"] : 3;
		elseif ($partita["gioco"]==="connect4"):
			$partita["board"]=7;
			$partita["wincase"]=4;
		endif;
		$partita["playerx"]=$_SESSION["utente"];
		$partita["playero"]="";
		$partita["turno"]=Q4_TRIS_PLAYER_ONE;
		$partita["cellevincenti"]=array();
		$partita["scacchiera"]=tris_scacchiera_vuota($partita);

		return $partita;
	}

function tris_check_referer()
{
	if (array_key_exists("HTTP_REFERER",$_SERVER)):
		if (preg_match("/partita/", $_SERVER["HTTP_REFERER"])):											// guarda se esiste "partita" nell'url
			$parametri=parse_url($_SERVER["HTTP_REFERER"], PHP_URL_QUERY);            // ottieni i parametri
			parse_str($parametri, $parametripartita);
			$codicepartita=$parametripartita["partita"];
			$quellapartita=tris_carica_partita($codicepartita);
			$quellapartita=str_replace($_SESSION["utente"], "", $quellapartita);
			if (is_file("./partite/".$codicepartita.".json")):
				tris_salva_partita($quellapartita);
			endif;
		endif;
	endif;
}

function tris_cancella_partita($codice)
{
	unlink("./partite/".$codice.".json");
}

function tris_scacchiera_vuota($partita)
{
  if ($partita["gioco"] === "tris"):
    return array_fill(0, $partita["board"], array_fill(0, $partita["board"], ""));
  elseif ($partita["gioco"] === "connect4"):
    return array_fill(0, 6, array_fill(0, 7, ""));
  endif;
}

function tris_mossa($partita,$riga,$colonna)
{
  if (array_key_exists($riga,$partita["scacchiera"])):	//la riga esiste?
  	if (array_key_exists($colonna,$partita["scacchiera"][$riga])): //la cella esiste?
  		if ($partita["scacchiera"][$riga][$colonna]==""): //la cella è vuota?
  			//mettere il simbolo
  			$partita["scacchiera"][$riga][$colonna]=tris_turno_attuale($partita["scacchiera"]);
        $giocatore=$partita["scacchiera"][$riga][$colonna];
  			$partita["cellevincenti"]=tris_celle_vincenti($partita,$riga,$colonna,$giocatore);
  			$partita["turno"]=tris_turno_attuale($partita["scacchiera"]);
  		endif;
  	endif;
  endif;

  return $partita;
}

function reset_counters()
{
  // $clean = array("count", 'count'.Q4_TRIS_PLAYER_ONE, 'count'.Q4_TRIS_PLAYER_TWO, 'countTie');
  // foreach($_SESSION as $key => $value):
  //     if(in_array($key, $clean)):
  //       unset($_SESSION[$key]);
  //     endif;
  // endforeach;
}

function tris_turno_attuale($scacchiera)
{
  $numerox=0;
  $numeroo=0;
  $numerovuote=0;
  foreach($scacchiera as $riga):
    foreach($riga as $cella):
      $numerox = ($cella == Q4_TRIS_PLAYER_ONE ? ++$numerox : $numerox);
      $numeroo = ($cella == Q4_TRIS_PLAYER_TWO ? ++$numeroo : $numeroo);
      $numerovuote=($cella==Q4_TRIS_PLAYER_NONE?++$numerovuote:$numerovuote);
    endforeach;
  endforeach;
  if ($numerovuote==0):
    $turno=Q4_TRIS_PLAYER_NONE;
  else:
    $turno=($numerox>$numeroo ? Q4_TRIS_PLAYER_TWO : Q4_TRIS_PLAYER_ONE);
  endif;

  return $turno;
}

function tris_celle_vincenti($partita,$i,$s,$giocatore)
{
	$scacchiera=$partita["scacchiera"];
  $cellevincenti[]=array($i,$s);
  $possibilità=array("y","z","x","t");

  foreach ($possibilità as $direzione):
    for ($verso=0; $verso<2; $verso++):
      list($row,$col)=scansiona_dove($direzione,$verso,$i,$s);
        for ($cella=0; $cella < $partita["wincase"]-1 ; $cella++):
          if ((array_key_exists($row,$scacchiera))
          && (array_key_exists($col,$scacchiera[$row]))
          && ($scacchiera[$row][$col]==$giocatore)):
            $cellevincenti[]=array($row,$col);
            list($row,$col)=scansiona_dove($direzione,$verso,$row,$col);
          else:
            break;
          endif;
        endfor;
    endfor;
    if (count($cellevincenti)<$partita["wincase"]):
      unset($cellevincenti);
      $cellevincenti[]=array($i,$s);
    endif;
  endforeach;
  if (count($cellevincenti)<$partita["wincase"]):
    $cellevincenti=array();
  endif;

  return $cellevincenti;
}

function scansiona_dove($direzione,$verso,$a,$b)
{
  switch ($direzione):
    case "y":
      $a=($verso==0)? ++$a : --$a;
      break;
    case "z":
      $a=($verso==0)? ++$a : --$a;
      $b=($verso==0)? ++$b : --$b;
      break;
    case "x":
      $b=($verso==0)? ++$b : --$b;
      break;
    case "t":
      $a=($verso==0)? --$a : ++$a;
      $b=($verso==0)? ++$b : --$b;
      break;
  endswitch;
return array($a,$b);
}

function tris_stampa_scacchiera($partita)
{
	ob_start();

  $codice=$partita["codice"];
	$scacchiera=$partita["scacchiera"];
	$turno=$partita["turno"];
	$cellevincenti=$partita["cellevincenti"];

  $vincitore=array_key_exists(0, $cellevincenti)?
    $scacchiera[$cellevincenti[0][0]][$cellevincenti[0][1]]:
    Q4_TRIS_PLAYER_NONE;
																																// scrive tutto in uno spazio di buffer e
?>
  <?php	if ($vincitore==Q4_TRIS_PLAYER_NONE):
  			  if ($turno==Q4_TRIS_PLAYER_NONE):?>
  			<p>Pareggio.</p>
  <?php		else:?>
  			<p>Turno : <?=$turno;?> (<?=$partita[$turno==Q4_TRIS_PLAYER_ONE?"playerx":"playero"];?>)</p>
  <?php		endif;
  		  else:?>
  			<p class="winner">Vincitore : <?=$vincitore;?>
  <?php	endif;?>

        <table>
  <?php foreach($scacchiera as $numriga => $riga):?>
          <tr>
  <?php   foreach($riga as $numcella => $cella):
        			$classecella=(array_search(array($numriga,$numcella),$cellevincenti)!==false ?
        				" class=\"vince\"":
        				"");
        		?>
			     <td<?=$classecella;?>>
  <?php     if ($cella==Q4_TRIS_PLAYER_NONE):
     				//se la cella è vuota
     				switch ($turno):
     					case (Q4_TRIS_PLAYER_ONE):
     						$abile=($_SESSION["utente"]==$partita["playerx"]);
     						break;
     					case (Q4_TRIS_PLAYER_TWO):
     						$abile=($_SESSION["utente"]==$partita["playero"]);
     						break;
     					default:
     						$abile=false;
     				endswitch;
     				if ($abile && $vincitore==Q4_TRIS_PLAYER_NONE && $turno!=Q4_TRIS_PLAYER_NONE):?>
              <form method="post">
                <input type="hidden" name="<?=Q4_TRIS_ROW;?>" value="<?=$numriga;?>">
                <input type="hidden" name="<?=Q4_TRIS_COL;?>" value="<?=$numcella;?>">
                <button type="submit" name="<?=Q4_TRIS_CMD;?>" value="<?=Q4_TRIS_MOVE;?>"></button>
              </form>
  <?php       endif;
            else: //se la cella è piena?>
              <?=$cella;?>
  <?php     endif;?>
            </td>
  <?php   endforeach;?>
          </tr>
  <?php endforeach;?>
        </table>

  <?php
		$html=ob_get_clean();
		// $stili=array(
		//
		// );
		$scripthead=array(
			"base" => "<script src=\"js/base.js\"></script>",
			"jq" => '<script src="https://code.jquery.com/jquery-3.5.1.min.js"
		  				integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
			 				crossorigin="anonymous">'
		);																																				// '<script src="js/beforeunload.js"></script>'
		$scriptbody=array(
			"partita" => "<script src=\"js/partita.js\"></script>"
		);

		return array(
			"html" => $html,
			// "stili" => $stili,
			"scripthead" => $scripthead,
			"scriptbody" => $scriptbody
		);
}
