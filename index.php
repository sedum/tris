<?php
	//avvio la sessione
	session_start();

	//solo con login
	if (!array_key_exists("utente",$_SESSION)):
		header("Location: ./login.php");
		die();
	endif;

	//carico funzioni tris
	require "./lib/common.php";

	// analisys of inputs
  tris_analyse_get();
  tris_analyse_post();

	tris_check_referer();     																										// guarda se utente proviene da una partita, e nel caso lo cancella dai giocatori di questa

	//leggi le partite disponibili
	$codici=tris_elenca_partite();

?><!DOCTYPE html>
<html lang="it">
<head>
	<?php	require_once "./inc/_meta.php";?>
	<meta name="keywords" content="">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=3, minimum-scale=0.5">
	<title>Little games</title>
	<script src="js/base.js"></script>
	<script src="https://code.jquery.com/jquery-3.5.1.min.js"
				  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
				  crossorigin="anonymous">
	</script>
</head>

<body>
	<main class="tris index">
		<h1><?=$_SESSION["utente"];?></h1>

		<section>
			<div id="anteprima"></div>
		</section>

		<section>
			<div id="elencopartite">
				<ol>
				<?php foreach($codici as $codice):?>
				<?php   $partita=tris_carica_partita($codice);
								$giocatori=array();
								foreach ($partita as $key => $value):
									if (preg_match("/player/", $key) && $value!=""):
										$giocatori[]=$value;
									endif;
								endforeach;
								if ($giocatori==array()):
									tris_cancella_partita($codice);																	// se non sono presenti giocatori la partita va cancellata
									break;
								else:?>
					<li>
						<ul>
							<li><a href="partita.php?partita=<?=$codice;?>&<?=Q4_TRIS_CMD;?>=<?=Q4_TRIS_JOIN;?>">Gioca</a></li>
							<li><a data-idpartita="<?=$codice;?>" href="partita.php?partita=<?=$codice;?>">Osserva</a></li>
							<li><?=count($giocatori)?>p
								<ul>
								<?php foreach ($giocatori as $users):?>
									<li><?=$users;?></li>
								<?php	endforeach;?>
								</ul>
							</li>
						</ul>
					</li>
				<?php		endif;
							endforeach;?>
				</ol>
				<a href="logout.php">Logout</a>
			</div>
			</section>

			<section>
				 <?php require_once "./inc/_form-tris.php";?>
			</section>

		</main>

		<script src="js/attiva.js"></script>
		<script src="js/index.js"></script>

	</body>
</html>
