<?php
// //avvio la sessione
// session_start();

// // solo con login
// if (!array_key_exists("utente",$_SESSION)):
//   header("Location: ./login.php");
//   die();  // php muori qui!
//   // exit();
// endif;

//carico funzioni tris
require_once "./lib/common.php";
//
//leggi le partite disponibili
$codici=tris_elenca_partite();
?>
			  <ol>
				<?php foreach($codici as $codice):?>
				<?php   $partita=tris_carica_partita($codice);
								$giocatori=array();
								foreach ($partita as $key => $value):
									if (preg_match("/player/", $key) && $value!=""):
										$giocatori[]=$value;
									endif;
								endforeach;
								if ($giocatori==array()):
									tris_cancella_partita($codice);																	// se non sono presenti giocatori la partita va cancellata
									break;
								else:?>
					<li>
						<ul>
							<li><a href="partita.php?partita=<?=$codice;?>&<?=Q4_TRIS_CMD;?>=<?=Q4_TRIS_JOIN;?>">Gioca</a></li>
							<li><a data-idpartita="<?=$codice;?>" href="partita.php?partita=<?=$codice;?>">Osserva</a></li>
							<li><?=count($giocatori)?>p
								<ul>
								<?php foreach ($giocatori as $users):?>
									<li><?=$users;?></li>
								<?php	endforeach;?>
								</ul>
							</li>
						</ul>
					</li>
				<?php		endif;
							endforeach;?>
				</ol>
				<a href="logout.php">Logout</a>
        <script src="js/attiva.js"></script>
<?php
