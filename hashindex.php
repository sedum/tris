<?php
  //avvio la sessione
  session_start();

  // solo con login
  if (!array_key_exists("utente",$_SESSION)):
    header("Location: ./login.php");
    die();  // php muori qui!
    // exit();
  endif;

  // load game's functions
  require_once "./lib/common.php";

  $hashindex=tris_hash_index();

  echo json_encode($hashindex);
