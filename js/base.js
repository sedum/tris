// script di base
"use strict";                                                                   // se messa come prima riga reale serve a spostare/commutare il browser nella modalità strict
                                                                                // non puoi usare variabili non dichiarate
                                                                                // in js lo scope è importante, perché pieno di variabili (che riguardano il DOM) già definite

                                                                                // document="";  distrugge l'accesso al DOM
                                                                                // pippo=""; name="";
                                                                                // window.name
                                                                                // in js non strict ogni variabile dichiarata diventa anche una proprietà dell'oggetto window
// lettura larghezza viewport
function ottieniLarghezzaViewport()
{
  let largview=top.document.documentElement.clientWidth;
  return largview;
}

// lettura altezza viewport
function ottieniAltezzaViewport()
{
  let altview=top.document.documentElement.clientHeight;
  return altview;
}

// modifica action della form
function aggiornaActionLogin(altview,largview)
{
  let maschera=document.querySelectorAll("form");
  maschera[0].action="?alt="+altview+"&larg="+largview;
}
                                                                                // una funzione viene chiamata o per il suo risultato(return) o per i suoi effetti collaterali,
                                                                                // non scrivete funzioni che facciano entrambe
function leggiDimViewport()
{
  let larvp=ottieniLarghezzaViewport();
  let altvp=ottieniAltezzaViewport();
  aggiornaActionLogin(altvp,larvp);
}

function anteprimaScacchiera(evento)                                            // quando c'è una promise viene restituito un parametro con le info su quell'evento
{
  let codicepartita=$(evento.target).data("idpartita");                        // evento.target informazione sul tag nel quale l'evento si è verificato
  let urlpartita="scacchiera.php?partita="+codicepartita;
  $("#anteprima").load(urlpartita);
}

function caricaHash()
{
// let codicepartita=new URL(top.location,document.baseURI).searchParams.get("partita");
let indirizzo=new URL(top.location, document.baseURI);
let parametri=indirizzo.searchParams;
let codicepartita=parametri.get("partita");

let indhash="hash.php?partita="+codicepartita;
// $("div#parthash").load(indhash);
$.ajax({url:indhash,success:verificaHash});
}

function verificaHash(risposta)
{
  let hash=JSON.parse(risposta);
  if (hash!=precedentehash)
  {
    console.log("cambiatohash= "+hash);
    let indirizzo=new URL(top.location,document.baseURI);
    let parametri=indirizzo.searchParams;
    let codicepartita=parametri.get("partita");

    let indscac="scacchiera.php?partita="+codicepartita;
    $("#scacchiera").load(indscac);
    precedentehash=hash;
  }
  // console.log(hash);
}




function ricaricaHashIndex()
{
  $.ajax({url:"hashindex.php",success:verificaHashIndex});
}

function verificaHashIndex(risposta)
{
  let hash=JSON.parse(risposta);
  if (hash!=precedentehashindex)
  {
    console.log("cambiatohashindex= "+hash);
    $("#elencopartite").load("elencoindex.php");
    precedentehashindex=hash;
  }
}













//
//
// let largview=top.document.documentElement.clientWidth;
// let altview=top.document.documentElement.clientHeight;
//
// let maschera=document.querySelectorAll("form");
// maschera[0].action="?alt="+largview+"&larg="+altview;
//
// if (maschera[0].target!="_self" && maschera[0].target!="_blank"){
// 	largview=top.frames[maschera[0].target].document.documentElement.clientWidth;
// 	altview=top.frames[maschera[0].target].document.documentElement.clientHeight;
// };
