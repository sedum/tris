<?php
  //avvio la sessione
  session_start();

  // solo con login
  if (!array_key_exists("utente",$_SESSION)):
    header("Location: ./login.php");
    die();  // php muori qui!
    // exit();
  endif;

  // load game's functions
  require_once "./lib/common.php";

  // games default values and specific functions
  // tris_game_defaults();

  //carico la partita!
	$partita=array();
	if (array_key_exists("partita",$_GET)):
		$partita=tris_carica_partita($_GET["partita"]);
	endif;
	if ($partita==array()):
		$partita=tris_nuova_partita();
	endif;

  // analisys of inputs
  tris_analyse_get();
  tris_analyse_post();

  $codice=tris_salva_partita($partita);
  $scacchierapreparata=tris_stampa_scacchiera($partita);
  // $_SESSION["scacchierapreparata"]=$scacchierapreparata;
  // $scriptheadpagina=array_merge($scripthead);

?><!DOCTYPE html>
<html lang=it>
<head>
  <title><?= ucfirst($partita["gioco"])?></title>
  <?php	require_once "./inc/_meta.php";?>
  <meta name="keywords" content="">
  <meta name="description" content="0">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=3, minimum-scale=0.5">
  <link href="./css/_<?=$partita["gioco"];?>.css" rel="stylesheet" type="text/css">
<?php echo implode("\n", $scacchierapreparata["scripthead"]);?>
	</script>
</head>
<body>
  <main class="tris">

    <section>
      <a href="index.php">Elenco partite</a>
    </section>
    <section>
    <h1><?=$partita["gioco"]==="tris" ? "Tris" : "Connect4";?></h1>
    <div id="scacchiera">
<?php
  // stampo la scacchiera
  echo $scacchierapreparata["html"];
?>
  </div>
  </section>
  <section>
  </section>

  </main>

<?php echo implode("\n", $scacchierapreparata["scriptbody"]);?>
</body>
</html>
