<?php
	//avvio la sessione
	session_start();

	//carico funzioni tris
	require_once "./lib/common.php";

	tris_check_referer();     																										// guarda se utente proviene da una partita, e nel caso lo cancella dai giocatori di questa

	//carico funzioni utenti
	require_once "./inc/_utenti.php";
	$metaref="";
	$messaggio="";
	if (array_key_exists("utente",$_SESSION)):
		header("Location: index.php");
		die();
	else:
		if (array_key_exists("utente",$_POST)):
			if (array_key_exists("password",$_POST)):
				if (utenti_aggiungi($_POST["utente"],$_POST["password"])):
					$_SESSION["utente"]=$_POST["utente"];
					$messaggio="Grazie per esserti registrato, attendi...";
					$metaref="<meta http-equiv=\"refresh\" content=\"5;./index.php\">";
				else:
					$messaggio="Nome utente non valido.";
				endif;
			endif;
		endif;
	endif;

?><!DOCTYPE html>
<html lang="it">
	<head>
<?php	require_once "./inc/_meta.php";?>
		<meta name="keywords" content="">
		<?=$metaref;?>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=3, minimum-scale=0.5">
		<title>Tris</title>
	</head>
	<body>
		<main class="tris form">
			<h1>Registrazione</h1>
<?php 	if ($metaref==""):?>
			<form method="post">
				<label>
					Utente:
					<input name="utente">
				</label>
				<label>
					Password:
					<input name="password">
				</label>
				<button type="submit">Invia</button>
			</form>
<?php	endif;?>
			<p class="errore">
				<?=$messaggio;?>
<?php if (strpos($messaggio, 'attendi')):?>
			<p>Se non vieni reindirizzata/o correttamente visita</p>
			<a href="login.php"> <?='http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).'/index.php';?></a>
<?php endif;?>
			</p>
		</main>
	</body>
</html>
