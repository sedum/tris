<?php
	//avvio la sessione
	session_start();

	//carico funzioni tris
	require_once "./lib/common.php";

	tris_check_referer();     																										// guarda se utente proviene da una partita, e nel caso lo cancella dai giocatori di questa

	//logout
	session_destroy();

	//messaggio
	$messaggio="<p>Logout effettuato</p><a href=\"login.php\">Fai login</a>";

?><!DOCTYPE html>
<html lang="it">
	<head>
<?php	require_once "./inc/_meta.php";?>
		<meta name="keywords" content="">
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=3, minimum-scale=0.5">
		<title>Tris</title>
	</head>
	<body>
		<main class="tris form">
			<h1>Logout</h1>
			<p><?=$messaggio;?></p>
		</main>
	</body>
</html>
