  <form class="tris-controls" method="post">

  <button name="<?=Q4_TRIS_CMD;?>" value="<?=Q4_TRIS_RESTART;?>" type="submit">Crea Nuova Partita</button>

  <legend>Gioco:
    <label>Tris
      <input type="radio" name="gioco" value="tris" selected checked></label>
    <label>Connect4
      <input type="radio" name="gioco" value="connect4"></label>
  </legend>

  <label><?= ucfirst(Q4_TRIS_BOARD);?> : </label>
  <select name="board">
    <option value="3" selected>3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
  </select>

  <label><?= ucfirst(Q4_TRIS_WIN);?> : </label>
  <select name="wincase">
    <option value="3" selected>3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
  </select>

  </form>
