<?php

function tris_scacchiera_vuota()
{
  if ($_SESSION["game"] == "tris"):
    return array_fill(0, $_SESSION[Q4_TRIS_BOARD], array_fill(0, $_SESSION[Q4_TRIS_BOARD], ""));
  elseif ($_SESSION["game"] == "connect4"):
    return array_fill(0, 6, array_fill(0, 7, ""));
  endif;
}

function tris_mossa($scacchiera,$riga,$colonna)
{
  if (array_key_exists($riga,$scacchiera)):  // la riga esiste?
    if (array_key_exists($colonna,$scacchiera[$riga])):  // la cella esiste?

      if ($scacchiera[$riga][$colonna] == ""):
        while ($riga < 6):
          if ( array_key_exists(++$riga,$scacchiera)
            && $scacchiera[$riga][$colonna] == "" ):
              continue;
          else:
            $scacchiera[--$riga][$colonna] = tris_turno_attuale($scacchiera);
            break;
          endif;
        endwhile;
      endif;

    endif;
  endif;
  return $scacchiera;
}

function tris_pc_move($scacchiera)
{
  $oriz = 0;
  $verti = rand(0, 6);
  $scacchiera = tris_mossa($scacchiera,$oriz,$verti);
  return $scacchiera;
}
