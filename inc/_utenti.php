<?php

function utente_valido($utente,$password)
{
	$valido=false;
	$utenti=utenti_esistenti();

	if (array_key_exists($utente,$utenti)):
		// l'utente esiste!
		if (password_verify($password,$utenti[$utente]["password"])):
			$valido=true;
		endif;
	endif;

	return $valido;
}

function utenti_esistenti()
{
	$utenti=array();
	if (is_file("./inc/cfg/utenti.json")):
		$jsonutenti=file_get_contents("./inc/cfg/utenti.json");
		$utenti=json_decode($jsonutenti,true);
	endif;

	return $utenti;
}

function utenti_aggiungi($nomeutente,$password)
{
	$utenti=utenti_esistenti();
	if (!array_key_exists($nomeutente,$utenti)):
		$utenti[$nomeutente]=array("password"=>password_hash($password, PASSWORD_DEFAULT));
	endif;
	utenti_salva($utenti);

	return $utenti;
}

function utenti_salva($utenti)
	{
		$jsonutenti=json_encode($utenti);
		file_put_contents("./inc/cfg/utenti.json",$jsonutenti);

		return $utenti;
	}
