<?php
	//avvio la sessione
	session_start();

	//carico funzioni tris
	require_once "./lib/common.php";

	tris_check_referer();     																										// guarda se utente proviene da una partita, e nel caso lo cancella dai giocatori di questa

	if (array_key_exists("utente",$_POST)):
		$_POST["utente"]=preg_replace('/[^a-zA-Z0-9]/', "", $_POST["utente"]);
	endif;

	//carico funzioni utenti
	require_once "./inc/_utenti.php";
	$metaref="";
	$messaggio="";
	if (array_key_exists("utente",$_SESSION)):
		$messaggio="Hai già fatto login.<br><a href=\"logout.php\">Fai logout</a>";
	else:
		if (array_key_exists("utente",$_POST)):
			if (array_key_exists("password",$_POST)):
				if (utente_valido($_POST["utente"],$_POST["password"])):
					$_SESSION["utente"]=$_POST["utente"];
					//header("Location: ./index.php");
					$messaggio="Grazie per aver fatto login, attendi...";
					$metaref="<meta http-equiv=\"refresh\" content=\"5;./index.php\">";
				else:
					$messaggio="Nome utente o password errati.";
				endif;
			endif;
		endif;
	endif;

?><!DOCTYPE html>
<html lang="it">
	<head>
<?php	require_once "./inc/_meta.php";?>
		<meta name="keywords" content="">
		<?=$metaref;
		?><meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=3, minimum-scale=0.5">
		<title>Tris</title>
		<script src="js/base.js"></script>
		<script
					  src="https://code.jquery.com/jquery-3.5.1.min.js"
					  integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
					  crossorigin="anonymous"></script>

	</head>
	<body>
		<main class="tris form">
			<h1>Login</h1>
<?php 	if (array_key_exists("utente",$_SESSION)): ?>
				<script>
					localStorage.setItem("utente","<?=$_SESSION["utente"];?>");
				</script>
<?php		else:?>
			<form method="post">
				<label>
					Utente:
					<input name="utente">
				</label>
				<label>
					Password:
					<input name="password" title="password" type="password">
				</label>
				<label class="togglePasswd">
				<input type="checkbox"></input>&nbsp;Show password
				</label>
				<button type="submit">Invia</button>
			</form>
<?php 	endif;?>
			<p class="errore"><?=$messaggio;?></p><br>
<?php if (strpos($messaggio, 'attendi')):?>
			<p>Se non vieni reindirizzata/o correttamente visita</p>
			<a href="login.php"> <?='http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).'/index.php';?></a>
<?php endif;
		?><a href="regis.php">Registrati</a>
		</main>
		<script src="js/attiva.js"></script>
		<script src="js/lib/togglePasswd.js"></script>
	</body>
</html>
